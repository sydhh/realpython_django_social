from django.contrib import admin
from django.contrib.auth.models import User, Group

from .models import Profile

# builtin Group model not use here in tuto
admin.site.unregister(Group)

# In order to simplify User creation, only require username (no password, ...)
# Warning: ONLY FOR TESTING PURPOSE

class ProfileInline(admin.StackedInline):
    model = Profile

class UserAdmin(admin.ModelAdmin):
    model = User
    # Only display the "username" field
    fields = ["username"]
    inlines = [ProfileInline]

admin.site.unregister(User)
admin.site.register(User, UserAdmin)

# register our model(s)
# admin.site.register(Profile)


