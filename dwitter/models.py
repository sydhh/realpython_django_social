from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    follows = models.ManyToManyField(
        "self",
        related_name="followed_by",
        symmetrical=False,
        blank=True
    )

    def __str__(self):
        return self.user.username

# signals


from django.db.models.signals import post_save


def create_profile(sender, instance, created, **kwargs):
    if created:
        user_profile = Profile(user=instance)
        user_profile.save()
        user_profile.follows.add(instance.profile)
        user_profile.save()



# Create a Profile for each new user.
post_save.connect(create_profile, sender=User)


# tests

# class Publication(models.Model):
#     title = models.CharField(max_length=30)
#
#     class Meta:
#         ordering = ['title']
#
#     def __str__(self):
#         return self.title
#
#
# class Article(models.Model):
#     headline = models.CharField(max_length=100)
#     publications = models.ManyToManyField(
#         Publication,
#         symmetrical=False,
#         blank=True,
#     )
#
#     class Meta:
#         ordering = ['headline']
#
#     def __str__(self):
#         return self.headline