
## Intro

* From:
  * https://realpython.com/django-social-network-1/
  * https://realpython.com/django-social-front-end-2/

* Src:
  *  https://github.com/realpython/materials

## Requirements

python 3.9
django 4

## Setup

python -m venv venv
venv/bin/python -m pip install django

## Dev

### shell + models User / Profile

* venv/bin/python ./manage.py shell
  * from django.contrib.auth.models import User
  * User.objects.create(username="bar")
  * ubar = User.objects.get(username="bar")
  * ubar.profile.follows.all()
  * ufoo = User.objects.get(username="foo")
  * ubar.profile.follows.add(ufoo.profile)
  * ubar.profile.follows.all()
